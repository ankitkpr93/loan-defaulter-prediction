# Loan-Defaulter-Prediction

The aim of this project is to build a Machine Learning model which is capable of predicting whether a loan will default (go bad) or not. The ‘PT_data’ csv file contains loan applications data. The description of the column names is given in ‘PT_data_info’ csv file (the descriptions of obvious names are skipped).

>>[Click here to open the Jupyter Notebook](Loan_Defaulter_Prediction.ipynb)

**Solution Approach**

**1.** Import all the required libraries and functions<br/>
**2.** Read dataset and perform exploratory analysis<br/>
**3.** Handling Outliers<br/>
**4.** Missing value imputation<br/>
**5.** SMOTE (Synthetic Minority Oversampling Technique)<br/>
**6.** Feature selection<br/>
**7.** Transform data<br/>
**8.** Transforming categorical values using One-Hot encoding<br/>
**9.** Split dataset<br/>
**10.** Training and Cross Validation<br/>

**Concepts/Libraries used**

- pandas
- SMOTE
- matplotlib
- scikit-Learn
- Decision Trees
- Random Forest
- Linear SVC
- XGBoost Classifier
- KNeighborsClassifier
- GridSearch
- Principal Component Analysis (PCA)
- Cross-Validation
- OneHot Encoding
- Feature Selection
- Outliers Removal
- Confusion Matrix
- Clasification Report
- ROC Curve
- Oversampling
